﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Crudditech.Models;

namespace Crudditech.Controllers
{
    public class sellersController : ApiController
    {
        private crudditechEntities db = new crudditechEntities();

        // GET: api/sellers
        public IQueryable<seller> Getseller()
        {
            return db.seller;
        }

        // GET: api/sellers/5
        [ResponseType(typeof(seller))]
        public IHttpActionResult Getseller(int id)
        {
            seller seller = db.seller.Find(id);
            if (seller == null)
            {
                return NotFound();
            }

            return Ok(seller);
        }

        // PUT: api/sellers/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putseller(int id, seller seller)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != seller.code)
            {
                return BadRequest();
            }

            db.Entry(seller).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!sellerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/sellers
        [ResponseType(typeof(seller))]
        public IHttpActionResult Postseller(seller seller)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.seller.Add(seller);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (sellerExists(seller.code))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = seller.code }, seller);
        }

        // DELETE: api/sellers/5
        [ResponseType(typeof(seller))]
        public IHttpActionResult Deleteseller(int id)
        {
            seller seller = db.seller.Find(id);
            if (seller == null)
            {
                return NotFound();
            }

            db.seller.Remove(seller);
            db.SaveChanges();

            return Ok(seller);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool sellerExists(int id)
        {
            return db.seller.Count(e => e.code == id) > 0;
        }
    }
}